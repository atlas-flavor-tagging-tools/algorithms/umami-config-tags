parameters: !include Preprocessing-parameters-HI.yaml

# Defining anchor with outlier cuts that are used over and over again
.outlier_cuts: &outlier_cuts
  - JetFitterSecondaryVertex_mass:
      operator: <
      condition: 25000
      NaNcheck: True
  - JetFitterSecondaryVertex_energy:
      operator: <
      condition: 1e8
      NaNcheck: True
  - JetFitter_deltaR:
      operator: <
      condition: 0.6
      NaNcheck: True

# Defining yaml anchors to be used later, avoiding duplication
.cuts_template_pythiadijets_train: &cuts_template_pythiadijets_train
  cuts:
    - eventNumber:
        operator: mod_6_<=
        condition: 3
    - pt_btagJes:
        operator: ">"
        condition: 2e4
    - *outlier_cuts

.cuts_template_validation: &cuts_template_validation
  cuts:
    - eventNumber:
        operator: mod_6_==
        condition: 4
    - *outlier_cuts

.cuts_template_test: &cuts_template_test
  cuts:
    - eventNumber:
        operator: mod_6_==
        condition: 5
    - *outlier_cuts

preparation:
  # Number of jets loaded per batch from the files for preparation.
  batchsize: 50000

  # Path to the .h5 ntuples from the h5 dumper.
  ntuples:
    pythiadijets:
      path: *ntuple_path
      file_pattern: /eos/user/s/shelena/input_data/bjet_filter/h5_2GeV/upgrade_*/*.h5
      #file_pattern: /eos/user/s/shelena/input_data/bjet_filter/h5_2GeV/upgrade_HI.2021-11-25T0304.2021-12-23-T103502_output.h5.JZ5.2GeV/user.shelena.27698112._000001.output.h5

  samples:
    training_pythiadijets_bjets:
      type: pythiadijets
      category: bjets
      n_jets: 10e7
      <<: *cuts_template_pythiadijets_train
      f_output:
        path: *sample_path
        file: mc16_5TeV-bjets_training_pythiadijets.h5

    training_pythiadijets_cjets:
      type: pythiadijets
      category: cjets
      # Number of c jets available in MC16d
      n_jets: 12745953
      <<: *cuts_template_pythiadijets_train
      f_output:
        path: *sample_path
        file: mc16_5TeV-cjets_training_pythiadijets.h5

    training_pythiadijets_ujets:
      type: pythiadijets
      category: ujets
      n_jets: 20e8
      <<: *cuts_template_pythiadijets_train
      f_output:
        path: *sample_path
        file: mc16_5TeV-ujets_training_pythiadijets.h5

    validation_pythiadijets:
      type: pythiadijets
      category: inclusive
      n_jets: 4e6
      <<: *cuts_template_validation
      f_output:
        path: *sample_path
        file: mc16_5TeV-inclusive_validation_pythiadijets.h5

    testing_pythiadijets:
      type: pythiadijets
      category: inclusive
      n_jets: 4e6
      <<: *cuts_template_test
      f_output:
        path: *sample_path
        file: mc16_5TeV-inclusive_testing_pythiadijets.h5

sampling:
  # Classes which are used in the resampling. Order is important.
  # The order needs to be the same as in the training config!
  class_labels: [ujets, cjets, bjets]

  # Decide, which resampling method is used.
  method: count

  # The options depend on the sampling method
  options:
    sampling_variables:
      - pt_btagJes:
          # bins take either a list containing the np.linspace arguments
          # or a list of them
          # For PDF sampling: must be the np.linspace arguments.
          #   - list of list, one list for each category (in samples)
          #   - define the region of each category.
          bins: [[0, 600000, 351], [650000, 6000000, 84]]

      - absEta_btagJes:
          # For PDF sampling: same structure as in pt_btagJes.
          bins: [0, 2.5, 10]

    # Decide, which of the in preparation defined samples are used in the resampling.
    samples:
      pythiadijets:
        - training_pythiadijets_bjets
        - training_pythiadijets_cjets
        - training_pythiadijets_ujets

    custom_njets_initial:
      # these are empiric values ensuring a smooth hybrid sample.
      # These values are retrieved for a hybrid ttbar + zprime sample for the count method!
      training_pythiadijets_bjets: 5.5e6
      training_pythiadijets_cjets: 11.5e6
      training_pythiadijets_ujets: 13.5e6

    # Fractions of ttbar/zprime jets in final training set. This needs to add up to one.
    fractions:
      pythiadijets: 1.0

    # number of training jets
    # For PDF sampling: this is the number of target jets to be taken (through all categories).
    #                   If set to -1: max out to target numbers (limited by fractions ratio)
    njets: 25e3

    # Bool, if track information (for DIPS etc.) are saved.
    save_tracks: True

    # Name of the track collection to use.
    tracks_name: "tracks"

    # this stores the indices per sample into an intermediate file
    intermediate_index_file: *intermediate_index_file
    #intermediate_index_file: indices.h5

    # for method: weighting
    # relative to which distribution the weights should be calculated
    weighting_target_flavour: 'bjets'

    # If you want to attach weights to the final files
    bool_attach_sample_weights: False

# Name of the output file from the preprocessing
outfile_name: *outfile_name
plot_name: HIJets_pythia_with_bjetfilter

# Variable dict which is used for scaling and shifting
var_file: *var_file

# Dictfile for the scaling and shifting (json)
dict_file: *dict_file

# compression for final output files (null/gzip)
compression: null

# save final output files with specified precision
precision: float16

# Options for the conversion to tfrecords
convert_to_tfrecord:
  chunk_size: 5000
