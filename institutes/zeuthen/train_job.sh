#!/bin/zsh
#
#(otherwise the default shell would be used)
#$ -S /bin/bash
#
# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
#
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi

own_name=$(basename $0)
train_config=
epochs=
live_save=false

instructions() {
  echo "Usage: $0 [-c CONFIG] [-e EPOCHS] [-s LIVE SAVE]" >&2
  echo " -a Training config" >&2
  echo " -e Number of epochs" >&2
  echo " -s optional boolean live_save=${live_save:-false}" >&2
}

# If the script must be executed with options, this checks if the number of arguments
# provided to the script is greater than 0
if [ $# -eq 0 ]; then
    instructions
    exit 1
fi

while getopts ":c:e:sh" option; do
    case "${option}" in
        c) train_config=${OPTARG}
            ;;
        e) epochs=${OPTARG}
            ;;
        s) live_save=true
            ;;
        h ) instructions
            exit 0
            ;;
        \?) echo "Option '-$OPTARG' is not a valid option." >&2
            instructions
            exit 1
            ;;
        : ) echo "Option '-$OPTARG' needs an argument." >&2
            instructions
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [[ -z "${train_config}" ]]; then
    instructions
fi

#
#############################################
#
# Options that start with #$s will be applied.
# For more details, man qsub.
# If running this script as a stand-alone,
# submit the script using qsub:
# qsub train_job.sh -c training-config.yml -e 200
##############################################
#
#(request gpu, this specific type is compatible with the latest umami singularity container)
#$ -l gpu_type=nvidia_nvidia_geforce_rtx_3090
#
#(job's maximum time)
#$-l h_rt=20:00:00
#
#(job's maximum resident memory usage)
#$ -l h_rss=64G
#
#(job's maximum scratch space usage in $TMPDIR)
#$ -l tmpdir_size=25G
#
#(stderr and stdout are merged together to stdout)
#$ -j y
#
#(execute the job from the current directory and not relative to your home directory)
#$ -cwd
#

# singularity should cache in $TMPDIR
SINGULARITY_CACHEDIR=$TMPDIR/.singularity
train_config_path=$SGE_O_WORKDIR/$train_config

epochs_option=""
if [[ ! -z "${epochs}" ]]; then
    epochs_option="-e ${epochs}"
fi

# copy results to working directory as they appear
if [[ "${live_save}" == true ]]; then
    singularity exec --nv docker://btagging/umami:latest-gpu python /umami/umami/train.py -c ${train_config_path} ${epochs_option}
# copy results when job finishes
else 
    cd $TMPDIR
    singularity exec --nv docker://btagging/umami:latest-gpu python /umami/umami/train.py -c ${train_config_path} ${epochs_option}
    results=$(ls -td -- */ | head -n 1)
    cp -r $results $SGE_O_WORKDIR/
fi

exit 0
